cmake_minimum_required(VERSION 3.3)
project(Retro3DSFML)

add_subdirectory(extlibs)

include_directories(${SFML_INCLUDE_DIR})
include_directories("include")
include_directories("extlibs/Retro3D/include")

include_directories("src")

# Gather c++ files
file(GLOB_RECURSE SRC_FILES 
    src/*.cpp
    src/*.h
)

add_executable(Retro3DSFML ${SRC_FILES})

target_link_libraries(Retro3DSFML Retro3D)
target_link_libraries(Retro3DSFML sfml-system sfml-window sfml-graphics)
