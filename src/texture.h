#pragma once
#include <vector>
#include <cstdint>

class Texture
{
public:
    std::vector<uint8_t> data;
    unsigned int width;
    unsigned int height;
};
