#include <iostream>
#include <cstring>
#include "SFML/Graphics.hpp"
#include "SFML/System.hpp"
#include "glm/vec2.hpp"
#include "glm/trigonometric.hpp"
#include "glm/gtx/norm.hpp"
#include "game_map.h"
#include "texture.h"
#include "retro3d.h"

#define FRAMEBUFFER_WIDTH 640
#define FRAMEBUFFER_HEIGHT 400
#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 800

glm::vec2 playerPosition;
glm::vec2 playerDirection;
float playerRotation = 0.0f;

const float movementSpeed = 4.0;
const float rotationSpeed = 3.0;

GameMap gameMap;

Texture* LoadTexture(const std::string& filepath)
{
    sf::Image image;
    if (image.loadFromFile(filepath))
    {
        const unsigned int size = image.getSize().x * image.getSize().y * 4;
        const uint8_t* pixels = image.getPixelsPtr();
        Texture* texture = new Texture();
        texture->data.resize(size);
        texture->width = image.getSize().x;
        texture->height = image.getSize().y;
        std::memcpy(texture->data.data(), pixels, size);
        return texture;
    }
    else
        return nullptr;
}

void HandleInput(float deltaTime)
{
    glm::vec2 movementDir;
    movementDir.y += sf::Keyboard::isKeyPressed(sf::Keyboard::W) ? 1.0f : 0.0f;
    movementDir.y -= sf::Keyboard::isKeyPressed(sf::Keyboard::S) ? 1.0f : 0.0f;
    movementDir.x += sf::Keyboard::isKeyPressed(sf::Keyboard::D) ? 1.0f : 0.0f;
    movementDir.x -= sf::Keyboard::isKeyPressed(sf::Keyboard::A) ? 1.0f : 0.0f;
    playerPosition += (playerDirection * movementDir.y) * movementSpeed * deltaTime;
    playerPosition += (glm::vec2(playerDirection.y, -playerDirection.x) * movementDir.x) * movementSpeed * deltaTime;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        playerRotation -= rotationSpeed * deltaTime;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        playerRotation += rotationSpeed * deltaTime;
    playerDirection = glm::normalize(glm::vec2(glm::cos(playerRotation), glm::sin(playerRotation)));
}

int main()
{
    sf::RenderWindow window(sf::VideoMode(sf::Vector2u(SCREEN_WIDTH, SCREEN_HEIGHT)), "Pseudo-3D Tutorial");
    sf::Texture frameBufferTexture;
    frameBufferTexture.create(sf::Vector2u(FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT));

    Retro3D::Renderer renderer;
    renderer.SetResolution(FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT);

    Texture* wallTexture = LoadTexture("../resources/textures/wall1.png");
    Texture* ceilingTexture = LoadTexture("../resources/textures/test4.png");
    Texture* floorTexture = LoadTexture("../resources/textures/floor1.png");
    Texture* spriteTexture = LoadTexture("../resources/textures/smiley.png");

    renderer.SetTexture(1, wallTexture->data.data(), wallTexture->data.size(), wallTexture->width, wallTexture->height);
    renderer.SetTexture(2, ceilingTexture->data.data(), ceilingTexture->data.size(), ceilingTexture->width, ceilingTexture->height);
    renderer.SetTexture(3, floorTexture->data.data(), floorTexture->data.size(), floorTexture->width, floorTexture->height);
    renderer.SetTexture(4, spriteTexture->data.data(), spriteTexture->data.size(), spriteTexture->width, spriteTexture->height);
    renderer.SetCeilingTexture(2);
    renderer.SetFloorTexture(3);

    SpriteData sprites[1] = { { 1.5, 5.0, 4 } };
    renderer.SetSprites(sprites, 1);

    gameMap.walls = {
        0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 1, 0, 0, 0, 0, 1,
        0, 0, 1, 0, 0, 0, 0, 1,
        1, 0, 0, 0, 1, 0, 0, 1,
        1, 0, 0, 0, 0, 0, 0, 1,
        1, 0, 1, 0, 0, 1, 0, 1,
        1, 0, 0, 0, 0, 1, 0, 1,
        1, 1, 1, 1, 1, 1, 1, 1
    };
    renderer.SetMap(gameMap.walls.data(), 8, 8);

    sf::Clock clock;
    sf::Time prevTime = clock.getElapsedTime();

    while (window.isOpen())
    {
        const sf::Time currTime = clock.getElapsedTime();
        const float deltaTime = (currTime - prevTime).asSeconds();
        prevTime = currTime;

        sf::Event event;
        while (window.pollEvent(event))
        {
        if (event.type == sf::Event::Closed)
            window.close();
        }

        HandleInput(deltaTime);
        renderer.SetCamera(playerPosition.x, playerPosition.y, playerDirection.x, playerDirection.y);
        
        renderer.Update(deltaTime);

        uint32_t frameBufferSize = renderer.GetFrameBufferSize();
        const uint8_t* frameBuffer = renderer.GetFrameBufferPtr();
        frameBufferTexture.update(frameBuffer);

        sf::Sprite sprite(frameBufferTexture);
        sprite.setScale(sf::Vector2f(static_cast<float>(SCREEN_WIDTH) / FRAMEBUFFER_WIDTH, static_cast<float>(SCREEN_HEIGHT) / FRAMEBUFFER_HEIGHT));

        window.clear();
        window.draw(sprite);
        window.display();
    }
    return 0;
}
